/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_freesplit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akorchyn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/28 18:04:30 by akorchyn          #+#    #+#             */
/*   Updated: 2018/10/28 18:06:14 by akorchyn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_freesplit(char **src)
{
	int	i;

	i = -1;
	if (!src || !*src)
		return ;
	while (src[++i])
		free(src[i]);
	free(src);
	src = NULL;
}
