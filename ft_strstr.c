/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akorchyn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 17:41:37 by akorchyn          #+#    #+#             */
/*   Updated: 2018/10/27 20:16:58 by akorchyn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	size_t	i;
	int		j;

	j = 0;
	i = ft_strlen((char *)needle);
	if ((char)needle[j] == '\0')
		return ((char *)haystack);
	while (haystack[j] != '\0')
	{
		if (ft_strncmp(haystack + j, needle, i) == 0)
			return ((char *)haystack + j);
		j++;
	}
	return (NULL);
}
