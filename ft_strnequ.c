/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akorchyn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/25 16:55:09 by akorchyn          #+#    #+#             */
/*   Updated: 2018/10/27 20:20:58 by akorchyn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strnequ(const char *s1, const char *s2, size_t n)
{
	int	res;

	if (!s1 || !s2)
		return (0);
	res = (ft_strncmp(s1, s2, n) == 0) ? 1 : 0;
	return (res);
}
