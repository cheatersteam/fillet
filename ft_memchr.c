/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akorchyn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/23 20:50:35 by akorchyn          #+#    #+#             */
/*   Updated: 2018/10/27 20:14:41 by akorchyn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	const char	*src;
	int			i;

	i = -1;
	src = s;
	while (++i < (int)n)
	{
		if (src[i] == (char)c)
			return (((char *)s) + i);
	}
	return (NULL);
}
