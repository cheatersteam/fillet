/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akorchyn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/23 21:52:07 by akorchyn          #+#    #+#             */
/*   Updated: 2018/10/27 20:15:49 by akorchyn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dest, const char *src, size_t n)
{
	int	i;

	i = -1;
	while (src[++i] != '\0' && i < (int)n)
		dest[i] = src[i];
	while (i < (int)n)
		dest[i++] = '\0';
	return (dest);
}
